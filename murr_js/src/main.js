import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import axios from "axios";
import 'element-plus/dist/index.css'
import App from './App.vue'
axios.defaults.baseURL = "http://127.0.0.1:8000";
const app = createApp(App)

app.use(ElementPlus)
app.mount('#app')