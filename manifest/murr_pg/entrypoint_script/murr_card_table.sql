create table if not exists murr_card_table

(
  id bigserial primary key,
  title char(224),
  content varchar,
  ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);