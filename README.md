<img src="readme/img/thumbnail.png" align="center" title="Murrengan network"/>

## Как соединить django, golang, vue js и postgresql
Друзья, привет! Меня зовут Егор Комаров и сегодня мы создадим веб приложение murr_card на популярных технологиях.

MurrCard
Название: мурр_кард, МуррКард, murr_card, MurrCard.
Описание: карточка с информацией. Содержит заголовок (строка до 224 символов) и контент (текст).

api:

Получить существующие murr_card. Через знак ? передам параметры: 
limit int - сколько мурр карточек получить за запрос
offset int - начиная с какого мурра выводить карточки

curl --request GET \
--url 'http://127.0.0.1:1991/murr_card/?limit=100&offset=0'


curl --request POST \
--url http://127.0.0.1:1991/murr_card/ \
--header 'Content-Type: application/json' \
--data '{"title": "Murr_card_title1", "content": "test1"}'


У меня есть проект на джанго с одним приложением.


### Этот репозиторий содержит код опенсорс социально сети Мурренган
В разработке применяется python, vue js, golang, postgresql

#### Интересно:
В данном проекте используется фреймворк django в совместной работе с чистым golang 1.17

#### Доступные фичи:
- создание мурр_кард. Мурр_кард - 


#### Установка локально

```bash

# Убедиться, что активировано виртуальное окружение с Python 3.9.5

git clone https://gitlab.com/Murrengan/m.git # копировать проект локально
pip install -r requirements.txt  # установка зависимостей python
python manage.py migrate # миграция базы данных
python manage.py runserver
```


Запустить бд в кубере
```bash
kubectl apply -f manifest/murr-pg.yaml
```

пробросить порт в постгрес

```bash
kubectl port-forward service/postgres 5432:5432
```

docker buildx build --platform=linux/amd64 -t registry.gitlab.com/murr_card/murr_back:0.3.0 . .
