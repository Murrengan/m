from django.contrib import admin

# from django_enum_choices.admin import EnumChoiceListFilter

from .models import MurrCard


@admin.register(MurrCard)
class MurrCardAdmin(admin.ModelAdmin):
    date_hierarchy = "timestamp"
    search_fields = [
        "title",
    ]
    list_display = ("title",)
