from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from rest_framework.routers import DefaultRouter

from .views import MurrCardViewSet

router = DefaultRouter()
router.register("", MurrCardViewSet, basename="murr_card")

urlpatterns = []

urlpatterns += router.urls
