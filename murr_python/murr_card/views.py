from rest_framework import status
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from murr_card.models import MurrCard
from murr_card.serializers import MurrCardSerializers


class MurrPagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = "murr_card_len"
    max_page_size = 50


class MurrCardViewSet(ModelViewSet):
    serializer_class = MurrCardSerializers
    pagination_class = MurrPagination

    def get_queryset(self):
        queryset = MurrCard.objects.all().order_by("-timestamp")

        return queryset

    def create(self, request, *args, **kwargs):

        data = request.data.copy()
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        return Response(serializer.data, status=status.HTTP_201_CREATED)
