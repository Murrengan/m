import os

name = "murr_back"
bind = "0.0.0.0:8000"
proc_name = "murr_back"
daemon = False
user = "root"
group = "root"

workers = 1

errorlog = "-"
accesslog = "-"
