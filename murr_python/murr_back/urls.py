from django.conf import settings
from django.conf.urls.static import static
from django.template.defaulttags import url
from django.urls import include, path
from rest_framework import routers
from django.contrib import admin
from murr_card import views

router = routers.DefaultRouter()

urlpatterns = [
    path("admin/", admin.site.urls),
    path("murr_card/", include("murr_card.urls")),
]
