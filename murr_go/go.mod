module rrrrr

go 1.17

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.5
	github.com/shopspring/decimal v1.3.1
)
