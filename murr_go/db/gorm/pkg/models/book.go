package models

import "time"

type MurrCard struct {
	Id        int    `json:"id" gorm:"primaryKey"`
	Title     string `json:"title"`
	Content   string `json:"author"`
	Timestamp time.Time
}
