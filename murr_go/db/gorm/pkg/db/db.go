package db

import (
	"fmt"
	"log"

	"github.com/tutorials/go/crud/pkg/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func Init() *gorm.DB {
	dbURL := "postgres://murr_postgres_user:murr_postgres_password@localhost:5432/murr_postgres_bd"

	db, err := gorm.Open(postgres.Open(dbURL), &gorm.Config{})

	if err != nil {
		log.Fatalln(err)
	}

	db.AutoMigrate(&models.MurrCard{})
	var mc models.MurrCard

	if result := db.First(&mc, 1); result.Error != nil {
		fmt.Println(result.Error)
	}
	//json.NewEncoder(w).Encode(book)

	return db
}
