module github.com/tutorials/go/crud

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	golang.org/x/crypto v0.0.0-20211115234514-b4de73f9ece8 // indirect
	gorm.io/driver/postgres v1.2.2
	gorm.io/gorm v1.22.3
)
