package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"log"
	"net/http"
	"time"
)

type MurrCard struct {
	Id          int       `json:"id"`
	Title       string    `json:"title"`
	Content     string    `json:"content"`
	DateCreated time.Time `json:"date_created"`
}

func main() {
	fmt.Println("Murrengan go server")
	r := mux.NewRouter()
	r.HandleFunc("/murr_card/", getMurrCard).Methods("GET")
	r.HandleFunc("/murr_card/", addMurrCard).Methods("POST")
	log.Fatal(http.ListenAndServe(":1991", r))
}
func getMurrCard(w http.ResponseWriter, r *http.Request) {
	fmt.Println("getMurrCard")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")
	var murrCards = murrCardPaginator(r.FormValue("limit"), r.FormValue("offset"))
	json.NewEncoder(w).Encode(murrCards)
}

func murrCardPaginator(limit string, offset string) []MurrCard {
	fmt.Println("murrCardPaginator")
	var murr_cards []MurrCard
	db, err := sql.Open("postgres", "postgres://murr_postgres_user:murr_postgres_password@localhost/murr_postgres_bd?sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	rows, err := db.Query("SELECT * FROM murr_card_murrcard ORDER BY timestamp LIMIT $1 OFFSET $2", limit, offset)
	for rows.Next() {

		mc := MurrCard{}
		err = rows.Scan(&mc.Id, &mc.Title, &mc.Content, &mc.DateCreated)
		if err != nil {
			log.Fatal(err)
		}
		murr_cards = append(murr_cards, mc)
	}
	return murr_cards

}
func addMurrCard(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")
	var murrCardData MurrCard
	json.NewDecoder(r.Body).Decode(&murrCardData)
	addNewMurrCard(murrCardData)
}

func addNewMurrCard(murrCardData MurrCard) {
	db, err := sql.Open("postgres", "postgres://murr_postgres_user:murr_postgres_password@localhost/murr_postgres_bd?sslmode=disable")

	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec("INSERT INTO murr_card_murrcard (title, content, timestamp) VALUES ($1,$2,$3)",
		murrCardData.Title,
		murrCardData.Content,
		murrCardData.DateCreated,
	)

	if err != nil {
		fmt.Println(err.Error())
		fmt.Println("addNewMurrCard crashed")
		return
	}
}
